#include <ros/ros.h>
#include <interactive_markers/interactive_marker_server.h>
#include <std_msgs/Float64.h>

ros::Publisher tilt_ctrl_pub;
std_msgs::Float64 kinect_tilt;

void processFeedbackUp(
    const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
    {
        if(feedback->event_type == visualization_msgs::InteractiveMarkerFeedback::MOUSE_DOWN)
        {
            kinect_tilt.data = std::min(kinect_tilt.data + 0.5, 31.0);
        }
    tilt_ctrl_pub.publish(kinect_tilt);
}

void processFeedbackDown(
    const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
    {
        if(feedback->event_type == visualization_msgs::InteractiveMarkerFeedback::MOUSE_DOWN)
        {
            kinect_tilt.data = std::max(kinect_tilt.data - 0.5, -31.0);
        }
    tilt_ctrl_pub.publish(kinect_tilt);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "interactive_marker");
    ros::NodeHandle nh;
    tilt_ctrl_pub = nh.advertise<std_msgs::Float64> ("tilt_angle", 1);
    // create an interactive marker server on the topic namespace simple_marker
    interactive_markers::InteractiveMarkerServer server("interactive_marker");
    // create an interactive marker for our server
    visualization_msgs::InteractiveMarker int_marker1;
    int_marker1.header.frame_id = "/camera_depth_frame";
    int_marker1.name = "my_marker1";
    // create a grey arrow marker
    visualization_msgs::Marker arrow_marker1;
    arrow_marker1.type = visualization_msgs::Marker::ARROW;
    arrow_marker1.pose.position.x = 0.5;
    arrow_marker1.pose.position.y = 0.0;
    arrow_marker1.pose.position.z = 1.0;
    arrow_marker1.pose.orientation.x = sin(M_PI/4.0);
    arrow_marker1.pose.orientation.y = 0.0;
    arrow_marker1.pose.orientation.z = cos(M_PI/4.0);
    arrow_marker1.pose.orientation.w = 0.0;
    arrow_marker1.scale.x = 0.3;
    arrow_marker1.scale.y = 0.1;
    arrow_marker1.scale.z = 0.1;
    arrow_marker1.color.r = 0.5;
    arrow_marker1.color.g = 0.5;
    arrow_marker1.color.b = 0.5;
    arrow_marker1.color.a = 1.0;

    visualization_msgs::InteractiveMarkerControl tilt_up_control;
    tilt_up_control.name = "tilt_up";
    tilt_up_control.interaction_mode =
    visualization_msgs::InteractiveMarkerControl::BUTTON;
    tilt_up_control.markers.push_back(arrow_marker1);
    tilt_up_control.always_visible = true;
    // add the control to the interactive marker
    int_marker1.controls.push_back(tilt_up_control);
    
    visualization_msgs::InteractiveMarker int_marker2;
    int_marker2.header.frame_id = "/camera_depth_frame";
    int_marker2.name = "my_marker2";
    // create a grey arrow marker
    visualization_msgs::Marker arrow_marker2;
    arrow_marker2.type = visualization_msgs::Marker::ARROW;
    arrow_marker2.pose.position.x = -0.5;
    arrow_marker2.pose.position.y = 0.0;
    arrow_marker2.pose.position.z = 1.0+0.3;
    arrow_marker2.pose.orientation.x = sin(-M_PI/4.0);
    arrow_marker2.pose.orientation.y = 0.0;
    arrow_marker2.pose.orientation.z = cos(-M_PI/4.0);
    arrow_marker2.pose.orientation.w = 0.0;
    arrow_marker2.scale.x = 0.3;
    arrow_marker2.scale.y = 0.1;
    arrow_marker2.scale.z = 0.1;
    arrow_marker2.color.r = 0.5;
    arrow_marker2.color.g = 0.5;
    arrow_marker2.color.b = 0.5;
    arrow_marker2.color.a = 1.0;

    visualization_msgs::InteractiveMarkerControl tilt_down_control;
    tilt_down_control.name = "tilt_down";
    tilt_down_control.interaction_mode =
    visualization_msgs::InteractiveMarkerControl::BUTTON;
    tilt_down_control.markers.push_back(arrow_marker2);
    tilt_down_control.always_visible = true;
    // add the control to the interactive marker
    int_marker2.controls.push_back(tilt_down_control);
    // add the interactive marker to our collection &
    // tell the server to call processFeedback() when feedback arrives for it
    server.insert(int_marker1, &processFeedbackUp);
    server.insert(int_marker2, &processFeedbackDown);
    // 'commit' changes and send to all clients
    server.applyChanges();

    ros::spin();
}
