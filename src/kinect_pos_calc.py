#!/usr/bin/env python

import rospy
import tf
from math import sin, cos
import numpy as np
from geometry_msgs.msg import PointStamped, Point, Vector3


class KinectCalc:
    def point_l_cb(self, corner):
        # x_k = np.array([[-corner.point.z],
        #                 [corner.point.x],
        #                 [-corner.point.y]])
        # x_w = np.array([[self.ref_point[0].x],
        #                 [self.ref_point[0].y],
        #                 [self.ref_point[0].z]])
        # kinect_position = x_w - np.dot(self.rotation_matrix, x_k)
        #
        # kinect_pos_msg = PointStamped()
        # kinect_pos_msg.header.stamp = rospy.Time.now()
        # kinect_pos_msg.point.x = kinect_position[0][0]
        # kinect_pos_msg.point.y = kinect_position[1][0]
        # kinect_pos_msg.point.z = kinect_position[2][0]
        # self.point_pub[0].publish(kinect_pos_msg)
        self.broadcaster.sendTransform((corner.point.x, corner.point.y, corner.point.z),
                                       tf.transformations.quaternion_from_euler(0, 0, 0),
                                       rospy.Time.now(),
                                       '/corner_l',
                                       '/camera_depth_optical_frame')
        (trans,rot) = self.listener.lookupTransform('/camera_depth_optical_frame', '/camera_depth_frame', rospy.Time(0))
        rot = tf.transformations.quaternion_multiply(rot, tf.transformations.quaternion_inverse(tf.transformations.quaternion_from_euler(self.roll, self.pitch, self.yaw)))
        self.broadcaster.sendTransform((0,0,0),
                                       rot,
                                       rospy.Time.now(),
                                       '/world',
                                       '/corner_l')


    def point_u_cb(self, corner):
        # x_k = np.array([[-corner.point.z],
        #                 [corner.point.x],
        #                 [-corner.point.y]])
        # x_w = np.array([[self.ref_point[1].x],
        #                 [self.ref_point[1].y],
        #                 [self.ref_point[1].z]])
        # kinect_position = x_w - np.dot(self.rotation_matrix, x_k)
        #
        # kinect_pos_msg = PointStamped()
        # kinect_pos_msg.header.stamp = rospy.Time.now()
        # kinect_pos_msg.point.x = kinect_position[0][0]
        # kinect_pos_msg.point.y = kinect_position[1][0]
        # kinect_pos_msg.point.z = kinect_position[2][0]
        # self.point_pub[1].publish(kinect_pos_msg)
        self.broadcaster.sendTransform((corner.point.x, corner.point.y, corner.point.z),
                                       tf.transformations.quaternion_from_euler(0, 0, 0),
                                       rospy.Time.now(),
                                       '/corner_u',
                                       '/camera_depth_optical_frame')
        (trans,rot) = self.listener.lookupTransform('/camera_depth_optical_frame', '/camera_depth_frame', rospy.Time(0))
        rot = tf.transformations.quaternion_multiply(rot, tf.transformations.quaternion_inverse(tf.transformations.quaternion_from_euler(self.roll, self.pitch, self.yaw)))
        self.broadcaster.sendTransform((0,0,0),
                                       rot,
                                       rospy.Time.now(),
                                       '/world_corner_u',
                                       '/corner_u')

    def rpy_cb(self, rpy):
        self.roll = rpy.x
        self.pitch = rpy.y
        self.yaw = rpy.z
        # self.rotation_matrix = np.array([[cos(self.pitch)*cos(self.yaw),  sin(self.roll)*sin(self.pitch)*cos(self.yaw) - cos(self.roll)*sin(self.yaw), cos(self.roll)*sin(self.pitch)*cos(self.yaw) + sin(self.roll)*sin(self.yaw)],
        #                                  [cos(self.pitch)*sin(self.yaw),  sin(self.roll)*sin(self.pitch)*sin(self.yaw) + cos(self.roll)*cos(self.yaw), cos(self.roll)*sin(self.pitch)*sin(self.yaw) - sin(self.roll)*cos(self.yaw)],
        #                                  [-sin(self.pitch),               sin(self.roll)*cos(self.pitch),                                              cos(self.roll)*cos(self.pitch)]])

    def __init__(self):
        self.roll = 0.;
        self.pitch = 0.;
        self.yaw = 0;
        # self.rotation_matrix = np.array([[cos(self.pitch)*cos(self.yaw),  sin(self.roll)*sin(self.pitch)*cos(self.yaw) - cos(self.roll)*sin(self.yaw), cos(self.roll)*sin(self.pitch)*cos(self.yaw) + sin(self.roll)*sin(self.yaw)],
        #                                  [cos(self.pitch)*sin(self.yaw),  sin(self.roll)*sin(self.pitch)*sin(self.yaw) + cos(self.roll)*cos(self.yaw), cos(self.roll)*sin(self.pitch)*sin(self.yaw) - sin(self.roll)*cos(self.yaw)],
        #                                  [-sin(self.pitch),               sin(self.roll)*cos(self.pitch),                                              cos(self.roll)*cos(self.pitch)]])

        self.ref_point = [Point(0., 0., 0.,), Point(0., 0., 0.27)]            # Reference point of the cube

        self.point_sub = [rospy.Subscriber("/corner_l", PointStamped, self.point_l_cb), rospy.Subscriber("/corner_u", PointStamped, self.point_u_cb)]
        self.angle_sub = rospy.Subscriber("/rpy", Vector3, self.rpy_cb)

        self.point_pub = [rospy.Publisher("/kinect_pos_l", PointStamped), rospy.Publisher("/kinect_pos_u", PointStamped)]

        self.broadcaster = tf.TransformBroadcaster()
        self.listener = tf.TransformListener()


if __name__ == '__main__':
    rospy.init_node('kinect_pos_calculator')

    kinect_calculator = KinectCalc()
    rospy.spin()