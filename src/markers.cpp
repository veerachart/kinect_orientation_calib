#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Point.h>
#include <std_msgs/Float64.h>
#include <math.h>
#include <pcl/ros/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/ModelCoefficients.h>

ros::Publisher markers_pub;
visualization_msgs::Marker normal_ref;

void coeff_cb(const geometry_msgs::Vector3ConstPtr& coeff)
{
  visualization_msgs::Marker normal_k;
  normal_k.header.frame_id = "/camera_depth_optical_frame";
  normal_k.type = visualization_msgs::Marker::ARROW;
  normal_k.ns = "normal_k";
  normal_k.id = 0;
  
  normal_k.action = visualization_msgs::Marker::ADD;
  
  normal_k.pose.orientation.w = 1.0;
  
  geometry_msgs::Point origin = geometry_msgs::Point();
  origin.x = 0;
  origin.y = 0;
  origin.z = 0;
  
  
  normal_k.header.stamp = ros::Time::now();
  geometry_msgs::Point tip = geometry_msgs::Point();
  tip.x = coeff->x;
  tip.y = coeff->y;
  tip.z = coeff->z;

  normal_k.points.push_back(origin);
  normal_k.points.push_back(tip);

  normal_k.scale.x = 0.05;
  normal_k.scale.y = 0.1;
  normal_k.scale.z = 0.1;
  
  normal_k.color.r = (coeff->x+1)/2.0;
  normal_k.color.g = (coeff->y+1)/2.0;
  normal_k.color.b = (coeff->z+1)/2.0;
  normal_k.color.a = 1.0;
    
  normal_k.lifetime = ros::Duration();
  
  visualization_msgs::Marker angle_diff;
  angle_diff.header.stamp = ros::Time::now();
  angle_diff.header.frame_id = "/camera_depth_frame";
  angle_diff.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  angle_diff.ns = "angle_diff";
  angle_diff.id = 3;
  
  angle_diff.action = visualization_msgs::Marker::ADD;
  
  angle_diff.pose.position.x = -1.0;
  angle_diff.pose.position.y = 0.0;
  angle_diff.pose.position.z = 0.0;
  angle_diff.pose.orientation.w = 1.0;

  angle_diff.scale.z = 0.15;
  
  angle_diff.color.r = 1.0;
  angle_diff.color.g = 1.0;
  angle_diff.color.b = 1.0;
  angle_diff.color.a = 1.0;
  
  char buff[20];
  geometry_msgs::Point a = normal_k.points[1];
  geometry_msgs::Point b = normal_ref.points[1];
  float angle = acos(a.x*b.x + a.y*b.y + a.z*b.z) * 180.0 / M_PI;
  sprintf(buff, "Angle diff\n    %.1f", angle);
  angle_diff.text = buff;
  
  angle_diff.lifetime = ros::Duration();
  
  markers_pub.publish(normal_k);
  markers_pub.publish(normal_ref);
  markers_pub.publish(angle_diff);
}

void coeff_top_cb(const pcl::ModelCoefficientsConstPtr& coeff)
{
  visualization_msgs::Marker top_k;
  top_k.header.frame_id = "/camera_depth_optical_frame";
  top_k.type = visualization_msgs::Marker::ARROW;
  top_k.ns = "top_k";
  top_k.id = 0;
  
  top_k.action = visualization_msgs::Marker::ADD;
  
  top_k.pose.orientation.w = 1.0;
  
  geometry_msgs::Point origin = geometry_msgs::Point();
  origin.x = 0;
  origin.y = -1.0;
  origin.z = 0;
  
  
  top_k.header.stamp = ros::Time::now();
  geometry_msgs::Point tip = geometry_msgs::Point();
  tip.x = origin.x+coeff->values[0];
  tip.y = origin.y+coeff->values[1];
  tip.z = origin.z+coeff->values[2];

  top_k.points.push_back(origin);
  top_k.points.push_back(tip);

  top_k.scale.x = 0.05;
  top_k.scale.y = 0.1;
  top_k.scale.z = 0.1;
  
  top_k.color.r = 1.0;
  top_k.color.g = 0;
  top_k.color.b = 0;
  top_k.color.a = 1.0;
    
  top_k.lifetime = ros::Duration();
  markers_pub.publish(top_k);
}

void coeff_left_cb(const pcl::ModelCoefficientsConstPtr& coeff)
{
  visualization_msgs::Marker left_k;
  left_k.header.frame_id = "/camera_depth_optical_frame";
  left_k.type = visualization_msgs::Marker::ARROW;
  left_k.ns = "left_k";
  left_k.id = 0;
  
  left_k.action = visualization_msgs::Marker::ADD;
  
  left_k.pose.orientation.w = 1.0;
  
  geometry_msgs::Point origin = geometry_msgs::Point();
  origin.x = -1.0;
  origin.y = 0;
  origin.z = 0;
  
  
  left_k.header.stamp = ros::Time::now();
  geometry_msgs::Point tip = geometry_msgs::Point();
  tip.x = origin.x+coeff->values[0];
  tip.y = origin.y+coeff->values[1];
  tip.z = origin.z+coeff->values[2];

  left_k.points.push_back(origin);
  left_k.points.push_back(tip);

  left_k.scale.x = 0.05;
  left_k.scale.y = 0.1;
  left_k.scale.z = 0.1;
  
  left_k.color.r = 0;
  left_k.color.g = 1.0;
  left_k.color.b = 0;
  left_k.color.a = 1.0;
    
  left_k.lifetime = ros::Duration();
  markers_pub.publish(left_k);
}

void coeff_right_cb(const pcl::ModelCoefficientsConstPtr& coeff)
{
  visualization_msgs::Marker right_k;
  right_k.header.frame_id = "/camera_depth_optical_frame";
  right_k.type = visualization_msgs::Marker::ARROW;
  right_k.ns = "right_k";
  right_k.id = 0;
  
  right_k.action = visualization_msgs::Marker::ADD;
  
  right_k.pose.orientation.w = 1.0;
  
  geometry_msgs::Point origin = geometry_msgs::Point();
  origin.x = 1.0;
  origin.y = 0;
  origin.z = 0;
  
  
  right_k.header.stamp = ros::Time::now();
  geometry_msgs::Point tip = geometry_msgs::Point();
  tip.x = origin.x+coeff->values[0];
  tip.y = origin.y+coeff->values[1];
  tip.z = origin.z+coeff->values[2];

  right_k.points.push_back(origin);
  right_k.points.push_back(tip);

  right_k.scale.x = 0.05;
  right_k.scale.y = 0.1;
  right_k.scale.z = 0.1;
  
  right_k.color.r = 0;
  right_k.color.g = 0;
  right_k.color.b = 1.0;
  right_k.color.a = 1.0;
    
  right_k.lifetime = ros::Duration();
  markers_pub.publish(right_k);
}

void tilt_cb (const std_msgs::Float64ConstPtr& tilt)
{
  visualization_msgs::Marker pitch_k;
  pitch_k.header.frame_id = "/camera_depth_frame";
  pitch_k.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  pitch_k.ns = "pitch_k";
  pitch_k.id = 1;
  
  pitch_k.action = visualization_msgs::Marker::ADD;
  
  pitch_k.pose.position.x = 0.0;
  pitch_k.pose.position.y = 0.0;
  pitch_k.pose.position.z = 1.0;
  pitch_k.pose.orientation.w = 1.0;
  
  pitch_k.header.stamp = ros::Time::now();

  pitch_k.scale.z = 0.15;
  
  pitch_k.color.r = 1.0;
  pitch_k.color.g = 1.0;
  pitch_k.color.b = 1.0;
  pitch_k.color.a = 1.0;
  
  char buff[20];
  sprintf(buff, "Pitch, sensor\n    %.1f", tilt->data);
  pitch_k.text = buff;
  
  pitch_k.lifetime = ros::Duration();
  markers_pub.publish(pitch_k);
}

int main( int argc, char** argv )
{
  ros::init(argc, argv, "basic_shapes");
  float ref_pitch = 15;
  if(argc==2)
  {
    ref_pitch = atof(argv[1]);
    ROS_INFO("Set reference angle to %.1f", ref_pitch);
  }
  else
  {
    ROS_INFO("Reference angle not given, use 15 degrees as default.");
  }
    
  ros::NodeHandle n;
    
  ros::Subscriber coeff_sub = n.subscribe ("normal", 1, coeff_cb);
  ros::Subscriber tilt_sub = n.subscribe("cur_tilt_angle", 1, tilt_cb);
  
  ros::Subscriber coeff1_sub = n.subscribe ("coefficients_1", 1, coeff_top_cb);
  ros::Subscriber coeff2_sub = n.subscribe ("coefficients_2", 1, coeff_left_cb);
  ros::Subscriber coeff3_sub = n.subscribe ("coefficients_3", 1, coeff_right_cb);
  
  markers_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 1);

  normal_ref.header.frame_id = "/camera_depth_frame";
  normal_ref.type = visualization_msgs::Marker::ARROW;
  normal_ref.ns = "normal_ref";
  normal_ref.id = 2;

  normal_ref.action = visualization_msgs::Marker::ADD;

  normal_ref.pose.orientation.w = 1.0;

  geometry_msgs::Point origin = geometry_msgs::Point();
  origin.x = 0;
  origin.y = 0;
  origin.z = 0;
  normal_ref.header.stamp = ros::Time::now();
  geometry_msgs::Point tip = geometry_msgs::Point();
  tip.x = -sin(ref_pitch*M_PI/180);
  tip.y = 0;
  tip.z = cos(ref_pitch*M_PI/180);
  normal_ref.points.push_back(origin);
  normal_ref.points.push_back(tip);

  normal_ref.scale.x = 0.05;
  normal_ref.scale.y = 0.1;
  normal_ref.scale.z = 0.1;

  normal_ref.color.r = 1.0;
  normal_ref.color.g = 1.0;
  normal_ref.color.b = 1.0;
  normal_ref.color.a = 0.75;

  normal_ref.lifetime = ros::Duration();

  ros::spin();
}
